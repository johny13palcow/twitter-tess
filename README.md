# README #

Twitter-like api learn/test app. 

Objectives:

- send tweet with specified hash (save into backend)

- search for tweets by specified tags

## Building

- produce ready-tu-run jar: `./gradlew build`

this will compile, run tests (with coverage), static analysis. 

If you want "html-readable" coverage report use `jacocoTestReport` tasks. Report
should be accessible as [build/reports/coverage/index.html](build/reports/coverage/index.html) 

Static analysis results are available [build/reports/spotbugs/main.html](build/reports/spotbugs/main.html)

- produce docker image: `./gradlew docker` 

this will build locally docker image `twitter-tess`

## Running

### Run from jar

Simply `java -jar java -jar build/libs/twitter-tess-0.0.2-SNAPSHOT.jar` with optional parameters.
This is springboot app so usual properties (host, port etc) apply.

Note: by default hsql database is placed within memory, override `spring.datasource.url` with valid hsql jdbc file url, eg
`spring.datasource.url=jdbc:hsqldb:file:/tmp/test`, you may do it on command line:

`java -jar build/libs/twitter-tess-0.0.2-SNAPSHOT.jar "--spring.datasource.url=jdbc:hsqldb:file:/tmp/jakis_plik"`

if you prefer env based configuration use `file` profile

`java -jar build/libs/twitter-tess-0.0.2-SNAPSHOT.jar --spring.profiles.active=file`

and set `JDBC_URL` env to valid hsql jdbc url
 
### Run from docker

Docker image requires the same env variable `JDBC_URL` to be set. HTTP rest is exposed over 8080 port.
Sample command to run docker image:

`docker run -it  -e JDBC_URL='jdbc:hsqldb:mem:twitter' -p 8080:8080 johny13palcow/twitter-tess`

You may probably want to attach persistent volume if you decide to use on-disk storage.

## API console (swagger)

Swagger is available on {{scheme//host:port}}/swagger-ui.html typically on developer machine or when you run docker image locally that would be 
http://localhost:8080/swagger-ui.html


