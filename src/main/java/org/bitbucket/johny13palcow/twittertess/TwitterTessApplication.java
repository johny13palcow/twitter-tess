package org.bitbucket.johny13palcow.twittertess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwitterTessApplication {

    public static void main(String[] args) {
        SpringApplication.run(TwitterTessApplication.class, args);
    }

}

