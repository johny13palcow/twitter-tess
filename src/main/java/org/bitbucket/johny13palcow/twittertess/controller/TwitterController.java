package org.bitbucket.johny13palcow.twittertess.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.johny13palcow.twittertess.model.SearchResult;
import org.bitbucket.johny13palcow.twittertess.model.SearchResultItem;
import org.bitbucket.johny13palcow.twittertess.model.Tweet;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApi;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApiException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.function.Function;

@RestController
@RequestMapping(value = "tweets")
@Slf4j
@Api(value = "Twitter-like access api")
public class TwitterController {

    private final TwitterApi api;

    public TwitterController(TwitterApi api) {
        this.api = api;
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Get specific tweet details")
    ResponseEntity<SearchResultItem> getTweet(
            @PathVariable
            @ApiParam(value = "tweet id", required = true)
                    Long id) {
        try {
            SearchResultItem result = api.getTweet(id);
            return ResponseEntity.ok(result);
        } catch (TwitterApiException e) {
            throw new ResponseStatusException(HttpStatus.valueOf(e.getTwitterStatusCode()), e.getMessage(), e);
        }

    }

    @GetMapping(value = "/search/byTag/{tag}", produces = "application/json")
    @ApiOperation(value = "Search twitter's posts by hashtag",
            notes = "We do return up to 10 items within single response, and optionally url to retrieve next page of items.")
    ResponseEntity<SearchResult> searchByTag(
            @PathVariable
            @ApiParam(value = "hashtag name without preceeding #.", required = true)
                    String tag,
            @RequestParam(required = false)
            @ApiParam(value = "requested page (of results).")
                    Integer page) {
        Integer pageToFetch = page != null ? page : 0;
        log.debug("search by tag: {} and page {}", tag, pageToFetch);
        UriComponentsBuilder uriBuilder = ServletUriComponentsBuilder.fromCurrentRequestUri();

        Function<Long, String> buildNextPageUri = id -> uriBuilder.queryParam("page", id).toUriString();

        try {
            SearchResult searchResult = api.searchByTag(tag, pageToFetch, buildNextPageUri);
            return ResponseEntity.ok(searchResult);
        } catch (TwitterApiException e) {
            throw new ResponseStatusException(HttpStatus.valueOf(e.getTwitterStatusCode()), e.getMessage(), e);
        }
    }

    @GetMapping(produces = "application/json")
    @ApiOperation(value = "Get user tweets",
            notes = "We do return up to 10 items within single response, and optionally url to retrieve next page of items.")
    SearchResult getUserTweets(@RequestParam(required = false)
                               @ApiParam(value = "requested page (of results).")
                                       Integer page) {
        Integer pageToFetch = page != null ? page : 0;
        log.debug("send tweet: {}", pageToFetch);

        UriComponentsBuilder uriBuilder = ServletUriComponentsBuilder.fromCurrentRequestUri();

        Function<Long, String> buildNextPageUri = id -> uriBuilder.queryParam("page", id).toUriString();

        try {
            return api.getTweets(pageToFetch, buildNextPageUri);
        } catch (TwitterApiException e) {
            throw new ResponseStatusException(HttpStatus.valueOf(e.getTwitterStatusCode()), e.getMessage(), e);
        }
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    @ApiOperation(value = "Post tweet")
    ResponseEntity<String> sendTweet(
            @RequestBody @Valid
            @ApiParam(value = "Tweet to be sent", required = true)
                    Tweet tweet) {
        log.debug("send tweet: {}", tweet);
        try {
            long id = api.sendTweet(tweet);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().pathSegment(String.valueOf(id)).build().toUri();
            return ResponseEntity.created(uri).build();
        } catch (TwitterApiException e) {
            throw new ResponseStatusException(HttpStatus.valueOf(e.getTwitterStatusCode()), e.getMessage(), e);
        }
    }
}
