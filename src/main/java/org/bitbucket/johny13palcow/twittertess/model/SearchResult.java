package org.bitbucket.johny13palcow.twittertess.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "SearchResult", description = "Twitter's search result")
public class SearchResult {
    @Builder.Default
    @ApiModelProperty(value = "List of returned items")
    private List<SearchResultItem> items = new ArrayList<>();
    @ApiModelProperty(value = "Optional uri of request to retrieve next page of results", allowEmptyValue = true)
    private String nextPageUrl;
}
