package org.bitbucket.johny13palcow.twittertess.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SearchResultItem")
public class SearchResultItem {
    @ApiModelProperty(value = "Tweet text value")
    private String text;
    @ApiModelProperty(value = "Tweet's author username")
    private String userName;
    @ApiModelProperty(value = "Tweet's id")
    private Long id;
    @ApiModelProperty(value = "Tweet's creation date")
    private Date date;
    @ApiModelProperty(value = "Tweet's hashtags")
    private List<String> hashTags;
}
