package org.bitbucket.johny13palcow.twittertess.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Single tweet to be sent.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Tweet", description = "Single tweet to be sent")
public class Tweet {

    @Size(max = 140, min = 1)
    @NotBlank
    @ApiModelProperty(value = "Tweet message, non-empty string up to 140 characters", required = true)
    private String text;
}
