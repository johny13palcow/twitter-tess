package org.bitbucket.johny13palcow.twittertess.storage.api;

import org.bitbucket.johny13palcow.twittertess.model.SearchResult;
import org.bitbucket.johny13palcow.twittertess.model.SearchResultItem;
import org.bitbucket.johny13palcow.twittertess.model.Tweet;

import java.util.function.Function;

/**
 * Storage access api.
 */
public interface TwitterApi {
    /**
     * sends single tweet
     *
     * @param tweet tweet to be sent
     * @return id of newly created tweet
     * @throws TwitterApiException in case of failure
     */
    long sendTweet(Tweet tweet) throws TwitterApiException;

    SearchResultItem getTweet(Long id) throws TwitterApiException;

    /**
     * searches for tweets by provided hashtag name. Up to 10 results is returned and (optional)
     * link to retrieve next page
     *
     * @param tag        hashtag name (without preceeding #)
     * @param pageToFetch      which page (of results) should be returned
     * @param uriBuilder function that given long produces string with url for next results
     * @return SearchResult instance with results.
     * @throws TwitterApiException in case of failure
     */
    SearchResult searchByTag(String tag, Integer pageToFetch, Function<Long, String> uriBuilder) throws TwitterApiException;

    /**
     * retrieves tweets in order from freshest to oldest. Up to 10 results is returned and (optional)
     * link to retrieve next page
     *
     * @param pageToFetch      which page (of results) should be returned
     * @param uriBuilder function that given long produces string with url for next results
     * @return SearchResult instance with results.
     * @throws TwitterApiException in case of failure
     */
    SearchResult getTweets(Integer pageToFetch, Function<Long, String> uriBuilder) throws TwitterApiException;
}
