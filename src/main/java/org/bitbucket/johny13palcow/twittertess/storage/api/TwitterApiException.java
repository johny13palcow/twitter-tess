package org.bitbucket.johny13palcow.twittertess.storage.api;

import lombok.Getter;

public class TwitterApiException extends Exception {

    @Getter
    final int twitterStatusCode;

    public TwitterApiException(String message, int statusCode, Throwable cause) {
        super(message, cause);
        this.twitterStatusCode = statusCode;
    }


}
