package org.bitbucket.johny13palcow.twittertess.storage.implementation;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.johny13palcow.twittertess.model.SearchResultItem;
import org.bitbucket.johny13palcow.twittertess.model.Tweet;
import org.bitbucket.johny13palcow.twittertess.model.SearchResult;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApi;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApiException;
import org.bitbucket.johny13palcow.twittertess.storage.model.TweetEntity;
import org.bitbucket.johny13palcow.twittertess.storage.respository.TweetRepository;
import org.bitbucket.johny13palcow.twittertess.storage.utils.TagExtractor;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Storage api access implementation.
 */
@Component
@Slf4j
public class TwitterService implements TwitterApi {

    private final TweetRepository tweetRepository;
    private final TagExtractor tagExtractor;

    public TwitterService(TweetRepository tweetRepository, TagExtractor tagExtractor) {
        this.tweetRepository = tweetRepository;
        this.tagExtractor = tagExtractor;
    }

    @Override
    public long sendTweet(Tweet tweet) throws TwitterApiException {
        try {
            TweetEntity jpaTweet = new TweetEntity();
            jpaTweet.setText(tweet.getText());
            jpaTweet.setTags(tagExtractor.extractTags(tweet.getText()));
            jpaTweet = tweetRepository.save(jpaTweet);
            return jpaTweet.getId();
        } catch (DataAccessException e) {
            throw new TwitterApiException("Data access failure", 503, e);
        }

    }

    @Override
    public SearchResultItem getTweet(Long id) throws TwitterApiException {

        Optional<TweetEntity> byId;
        try {
            byId = tweetRepository.findById(id);
        } catch (DataAccessException e) {
            throw new TwitterApiException("Data access failure", 503, e);
        }

        if (byId.isPresent()) {
            return byId.map(this::mapToSearchItem).get();
        }
        throw new TwitterApiException("Unable to find entry with specified id", 404, null);
    }

    @Override
    public SearchResult searchByTag(String tag, Integer requestedPage, Function<Long, String> uriBuilder) throws TwitterApiException {
        try {
            Page<TweetEntity> tweets = tweetRepository.findByTag(tag, PageRequest.of(requestedPage, 10, Sort.Direction.DESC, "id"));

            return convertResults(requestedPage, uriBuilder, tweets);
        } catch (DataAccessException e) {
            throw new TwitterApiException("Data access failure", 503, e);
        }
    }

    private SearchResult convertResults(Integer requestedPage, Function<Long, String> uriBuilder, Page<TweetEntity> tweets) {
        SearchResult searchResult = new SearchResult();
        if (!tweets.isEmpty()) {
            searchResult.setItems(tweets.get().map(this::mapToSearchItem).collect(Collectors.toList()));

            if (!tweets.isLast()) {
                searchResult.setNextPageUrl(uriBuilder.apply(requestedPage + 1L));
            }
        }
        return searchResult;
    }

    @Override
    public SearchResult getTweets(Integer requestedPage, Function<Long, String> uriBuilder) throws TwitterApiException {
        try {
            Page<TweetEntity> tweets = tweetRepository.findAll(PageRequest.of(requestedPage, 10, Sort.Direction.DESC, "id"));
            return convertResults(requestedPage, uriBuilder, tweets);
        } catch (DataAccessException e) {
            throw new TwitterApiException("Data access failure", 503, e);
        }
    }

    private SearchResultItem mapToSearchItem(TweetEntity status) {
        SearchResultItem item = new SearchResultItem();
        item.setDate(status.getCreatedAt());
        item.setText(status.getText());
        item.setId(status.getId());
        if (status.getTags() != null) {
            item.setHashTags(new ArrayList<>(status.getTags()));
        }
        return item;
    }
}
