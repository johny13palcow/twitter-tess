package org.bitbucket.johny13palcow.twittertess.storage.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class TweetEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    private Date createdAt;
    @Getter
    @Setter
    @Column(length = 140)
    private String text;
    @ElementCollection(fetch = FetchType.EAGER)
    @Getter
    @Setter
    private List<String> tags;

    @PrePersist
    public void prePersist() {
        createdAt = new Date();
    }
}
