package org.bitbucket.johny13palcow.twittertess.storage.respository;

import org.bitbucket.johny13palcow.twittertess.storage.model.TweetEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TweetRepository extends PagingAndSortingRepository<TweetEntity, Long> {

    @Query(value = "select a from TweetEntity a where :tag member of a.tags")
    Page<TweetEntity> findByTag(@Param(value = "tag") String tag, Pageable pageable);

//    @Override
//    @EntityGraph(value = "Tweet.tags", type = EntityGraph.EntityGraphType.FETCH)
//    Optional<Tweet> findById(Long aLong);
}
