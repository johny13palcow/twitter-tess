package org.bitbucket.johny13palcow.twittertess.storage.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Slf4j
public class TagExtractor {

    private static final Pattern p = Pattern.compile("#(\\w+)");

    public List<String> extractTags(String text) {
        List<String> tags = new ArrayList<>();
        Matcher m = p.matcher(text);
        while (m.find()) {
            tags.add(m.group(1));
        }
        log.debug("tags {}", tags);
        return tags;
    }
}
