package org.bitbucket.johny13palcow.twittertess.controller;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApi;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApiException;
import org.bitbucket.johny13palcow.twittertess.model.SearchResultItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class TwitterControllerGetSingleTweet {

    @MockBean
    TwitterApi twitterApi;

    @Value("${local.server.port}")
    private int serverPort;

    @Before
    public void before() {
        //config restassured port
        RestAssured.port = serverPort;
    }

    @Test
    public void getNoTweet() throws TwitterApiException {
        Mockito.when(twitterApi.getTweet(ArgumentMatchers.eq(0L))).thenThrow(new TwitterApiException("Unable to find entry with specified id", 404, null));
        Response response = given().get("/tweets/0");
        assertEquals(404, response.getStatusCode());
        SpringError springError = response.getBody().as(SpringError.class);
        assertEquals("Unable to find entry with specified id", springError.get("message"));
    }

    @Test
    public void testTweet() throws TwitterApiException {
        SearchResultItem item = SearchResultItem.builder().id(1L).text("test").date(new Date()).build();
        Mockito.when(twitterApi.getTweet(ArgumentMatchers.eq(1L))).thenReturn(item);
        Response response = given().get("/tweets/1");
        assertEquals(200, response.getStatusCode());
        assertEquals(item, response.getBody().as(SearchResultItem.class));
    }

}
