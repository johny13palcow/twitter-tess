package org.bitbucket.johny13palcow.twittertess.controller;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.johny13palcow.twittertess.model.SearchResult;
import org.bitbucket.johny13palcow.twittertess.model.SearchResultItem;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApi;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApiException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class TwitterControllerGetTweets {

    @MockBean
    TwitterApi twitterApi;

    @Value("${local.server.port}")
    private int serverPort;

    @Before
    public void before() {
        //config restassured port
        RestAssured.port = serverPort;
    }

    @Test
    public void getTweetsNoData() throws TwitterApiException {
        SearchResult empty = SearchResult.builder().build();
        Mockito.when(twitterApi.getTweets(ArgumentMatchers.eq(0), ArgumentMatchers.any())).thenReturn(empty);
        Response response = given().get("/tweets");
        assertSearchResult(empty, response);
    }

    @Test
    public void getTweetsPageData() throws TwitterApiException {
        SearchResult expected = SearchResult.builder().items(Arrays.asList(
                SearchResultItem.builder().id(2L).text("test 2").date(new Date()).build(),
                SearchResultItem.builder().id(1L).text("test 1").date(new Date()).build())
        ).build();
        Mockito.when(twitterApi.getTweets(ArgumentMatchers.eq(1), ArgumentMatchers.any())).thenReturn(expected);
        Response response = given().get("/tweets?page=1");
        assertSearchResult(expected, response);
    }

    private void assertSearchResult(SearchResult expected, Response response) {
        assertEquals(200, response.getStatusCode());
        SearchResult rs = response.getBody().as(SearchResult.class);
        assertEquals(expected, rs);
    }

    @Test
    public void testException() throws TwitterApiException {
        Mockito.when(twitterApi.getTweets(ArgumentMatchers.anyInt(), ArgumentMatchers.any())).thenThrow(new TwitterApiException("Backend failed", 503, null));
        Response response = given().get("/tweets");
        assertEquals(503, response.getStatusCode());
        SpringError springError = response.getBody().as(SpringError.class);
        assertEquals("Backend failed", springError.get("message"));
    }

}
