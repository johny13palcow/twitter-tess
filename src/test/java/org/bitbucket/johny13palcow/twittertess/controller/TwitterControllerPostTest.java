package org.bitbucket.johny13palcow.twittertess.controller;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApi;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApiException;
import org.bitbucket.johny13palcow.twittertess.model.Tweet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class TwitterControllerPostTest {

    @MockBean
    TwitterApi twitterApi;

    @Value("${local.server.port}")
    private int serverPort;

    @Before
    public void before() {
        //config restassured port
        RestAssured.port = serverPort;
    }

    @Test
    public void contextLoads() {
        //test whether app starts at all
    }

    @Test
    public void testEmptyTweet() {
        //no body
        Response response = given().contentType(ContentType.JSON).post("/tweets");
        assertEquals(400, response.getStatusCode());
    }

    @Test
    public void testTweetValidationNotBlank() {
        //empty tweet text
        Response response = given().contentType(ContentType.JSON).body(new Tweet()).post("/tweets");
        assertEquals(400, response.getStatusCode());
    }

    @Test
    public void testTweetValidationTooLong() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < 150; i++) {
            s.append(" ");
        }
        Tweet tweet = Tweet.builder().text(s.toString()).build();
        Response response = given().contentType(ContentType.JSON).body(tweet).post("/tweets");
        assertEquals(400, response.getStatusCode());
    }

    @Test
    public void testValidPost() throws TwitterApiException {
        Tweet tweet = Tweet.builder().text("My second tweet").build();
        Mockito.when(twitterApi.sendTweet(tweet)).thenReturn(123L);
        Response response = given().contentType(ContentType.JSON).body(tweet).post("/tweets");
        assertEquals(201, response.getStatusCode());
        assertEquals("http://localhost:" + serverPort + "/tweets/123", response.getHeader("Location"));
    }

}

