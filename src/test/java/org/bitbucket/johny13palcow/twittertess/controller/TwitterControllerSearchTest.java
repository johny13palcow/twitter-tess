package org.bitbucket.johny13palcow.twittertess.controller;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.johny13palcow.twittertess.model.SearchResult;
import org.bitbucket.johny13palcow.twittertess.model.SearchResultItem;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApi;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApiException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class TwitterControllerSearchTest {

    @MockBean
    TwitterApi twitterApi;

    @Value("${local.server.port}")
    private int serverPort;

    @Before
    public void before() {
        //config restassured port
        RestAssured.port = serverPort;
        //and parser
        RestAssured.defaultParser = Parser.JSON;
    }

    @Test
    public void testSearchNoTag() {
        Response response = given().get("/tweets/search/byTag/");
        assertEquals(404, response.getStatusCode());
    }

    @Test
    public void testSearchTag() throws TwitterApiException {
        Date now = new Date();
        List<SearchResultItem> firstPageItems = new ArrayList<>();
        for (long i = 1; i < 5; i++) {
            firstPageItems.add(SearchResultItem.builder().date(now).id(i).text("tweet #" + i).userName("user #" + i)
                    .hashTags(Arrays.asList("#TAG1", "#TAG2")).build());
        }
        SearchResult expectedResultPageNo1 = SearchResult.builder().items(firstPageItems).nextPageUrl("nextPageUrl").build();
        Mockito.when(twitterApi.searchByTag(ArgumentMatchers.eq("test"), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(expectedResultPageNo1);

        Response response = given().get("/tweets/search/byTag/test");
        assertEquals(200, response.getStatusCode());
        SearchResult searchResult = response.getBody().as(SearchResult.class);
        assertEquals(expectedResultPageNo1, searchResult);
    }

    @Test
    public void testApiException() throws TwitterApiException {
        Mockito.when(twitterApi.searchByTag(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenThrow(new TwitterApiException("Search failed, backend failed", 503, new Throwable()));
        Response response = given().get("/tweets/search/byTag/error");
        assertEquals(503, response.getStatusCode());
        SpringError springError = response.getBody().as(SpringError.class);
        assertEquals("Search failed, backend failed", springError.get("message"));
    }

}
