package org.bitbucket.johny13palcow.twittertess.service;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.johny13palcow.twittertess.model.SearchResult;
import org.bitbucket.johny13palcow.twittertess.model.SearchResultItem;
import org.bitbucket.johny13palcow.twittertess.model.Tweet;
import org.bitbucket.johny13palcow.twittertess.storage.api.TwitterApiException;
import org.bitbucket.johny13palcow.twittertess.storage.implementation.TwitterService;
import org.bitbucket.johny13palcow.twittertess.storage.model.TweetEntity;
import org.bitbucket.johny13palcow.twittertess.storage.respository.TweetRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class TwitterServiceTest {

    @Autowired
    TweetRepository repository;
    @Autowired
    TwitterService twitterService;

    @Test
    public void testPost() throws TwitterApiException {
        String tweetText = "test #tweet";
        long id = createTweet(tweetText);
        Optional<TweetEntity> byId = repository.findById(id);
        assertTrue(byId.isPresent());
        TweetEntity jpaTweet = byId.get();
        assertEquals(tweetText, jpaTweet.getText());
        assertNotNull(jpaTweet.getCreatedAt());
        assertEquals(1, jpaTweet.getTags().size());
        assertEquals("tweet", jpaTweet.getTags().get(0));
    }

    private long createTweet(String tweetText) throws TwitterApiException {
        Tweet tweet = Tweet.builder().text(tweetText).build();
        return twitterService.sendTweet(tweet);
    }

    @Test
    public void testGetSingle() throws TwitterApiException {
        String tweetText = "#another tweet";
        long id = createTweet(tweetText);
        SearchResultItem tweet = twitterService.getTweet(id);
        assertNotNull(tweet);
        assertEquals(tweetText, tweet.getText());
        assertNotNull(tweet.getHashTags());
        assertEquals(1, tweet.getHashTags().size());
        assertEquals("another", tweet.getHashTags().get(0));
    }

    @Test
    public void testSingleNotExisting() {
        try {
            twitterService.getTweet(666L);
        } catch (TwitterApiException e) {
            assertEquals(404, e.getTwitterStatusCode());
        }
    }


    @Test
    public void testGetListWithPages() throws TwitterApiException {
        //11 so we know there's next page
        for (int i = 0; i < 11; i++) {
            createTweet("Tweet No" + i);
        }
        SearchResult tweets = twitterService.getTweets(0, aLong -> "test" + aLong);
        assertNotNull(tweets);
        assertEquals("test1", tweets.getNextPageUrl());
        assertEquals(10, tweets.getItems().size());
        int idx = 0;
        int i = 10;
        do {
            assertEquals("Tweet No" + i, tweets.getItems().get(idx++).getText());
        } while (i-- > 1);
    }

    @Test
    public void testSearchNoResults() throws TwitterApiException {
        SearchResult searchResult = twitterService.searchByTag("abrakadabra", 0, aLong -> "test" + aLong);
        assertNotNull(searchResult);
        assertEquals(0, searchResult.getItems().size());
    }

    @Test
    public void testSearch() throws TwitterApiException {
        createTweet("Tweet No1, #tag1");
        createTweet("Tweet No2, #tag1");
        createTweet("Tweet No3, #tag2");
        createTweet("Tweet No4, #tag1");
        SearchResult searchResult = twitterService.searchByTag("tag1", 0, aLong -> "test" + aLong);
        assertNotNull(searchResult);
        assertEquals(3, searchResult.getItems().size());
        //match in Order
        assertEquals("Tweet No4, #tag1", searchResult.getItems().get(0).getText());
        assertEquals("Tweet No2, #tag1", searchResult.getItems().get(1).getText());
        assertEquals("Tweet No1, #tag1", searchResult.getItems().get(2).getText());

        searchResult = twitterService.searchByTag("tag2", 0, aLong -> "test" + aLong);
        assertNotNull(searchResult);
        assertEquals(1, searchResult.getItems().size());
        assertEquals("Tweet No3, #tag2", searchResult.getItems().get(0).getText());
    }
}